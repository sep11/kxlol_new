#ifndef __MAGICITE_GAME_DRAW_LAYER__
#define __MAGICITE_GAME_DRAW_LAYER__

#include "cocos2d.h"

class MagiciteGameDrawLine;

class MagiciteGameDrawLayer : public cocos2d::Layer
{
public:
    MagiciteGameDrawLayer();
    ~MagiciteGameDrawLayer();

    virtual bool init();
    CREATE_FUNC(MagiciteGameDrawLayer);

    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags);

    MagiciteGameDrawLine* createLine(const cocos2d::Vec2& pointA,
        const cocos2d::Vec2& pointB,
        float width,
        const cocos2d::Color4F& color);

    void destroyLine(MagiciteGameDrawLine* line);

    void clear();

private:
    cocos2d::DrawNode*                      _drawNode;
    std::vector<MagiciteGameDrawLine*>      _lineList;
};  

#endif //__MAGICITE_GAME_DRAW_LAYER__